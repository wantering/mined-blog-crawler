/**
 * Module dependencies
 */

var _ = require('lodash'),
    Promise = require('bluebird'),
    Waterline = require('waterline');


/**
 * Set up Waterline with the specified
 * models, connections, and adapters.

  @param options
    :: {Object}   adapters     [i.e. a dictionary]
    :: {Object}   connections  [i.e. a dictionary]
    :: {Object}   collections  [i.e. a dictionary]

  @param  {Function} cb
    () {Error} err
    () ontology
      :: {Object} collections
      :: {Object} connections

  @return {Waterline}
 */

module.exports = function bootstrap(options) {

    return new Promise(function(resolve, reject) {
        var adapters = options.adapters || {};
        var connections = options.connections || {};
        var collections = options.collections || {};


        _.each(adapters, function(def, identity) {
            // Make sure our adapter defs have `identity` properties
            def.identity = def.identity || identity;
        });


        // Instantiate Waterline
        var waterline = new Waterline();
        var extendedCollections = [];
        _.each(collections, function(def, identity) {

            // Make sure our collection defs have `identity` properties
            def.identity = def.identity || identity;
            var addMethod = function (method, methodName) {
                def[methodName] = method;
                def.attributes[methodName] = method;
            }
            
            addMethod(function (collName) {
                return waterline.collections[collName] || null;
            }, 'getCollection');

            _.each(options.globalMethods, addMethod);
            
            def.attributes = _.extend({}, options.globalAttributes || {}, def.attributes)

            // Add a default function that lets me save errors to the database

            def.attributes.saveError = function (stage, returnVal) {
                var handler = function (err) {
                    if (!err) var err = new Error('No error message received');
                    var errorObj = {
                        time: new Date(),
                        stage: stage,
                        message: err.message || err.toString()
                    };
                    this.problems = (this.problems || [])
                        .concat(errorObj)
                        .slice(-6);
                    if (this.state) this.state = 'error';
                    console.warn('Error:', this.id, errorObj.message);
                    var toReturn = returnVal ? Promise.resolve(returnVal) : Promise.reject(err);

                    return this.save().return(toReturn);
                }.bind(this);
                return handler;
            }

            // Fold object of collection definitions into an array
            // of extended Waterline collections.
            extendedCollections.push(Waterline.Collection.extend(def));
        });

        // Load the already extended collections
        extendedCollections.forEach(function(collection) {
            waterline.loadCollection(collection);
        });


        // Initialize Waterline
        // (and tell it about our adapters)
        waterline.initialize({
            adapters: adapters,
            connections: connections
        }, function(err, wl) {
            if (err) return reject(err);
            resolve(wl)
        });
    });
};