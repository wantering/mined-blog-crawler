require('babel/register');
var rss = require('./lib/rss'),
    settings = require('./settings'),
    Promise = require('bluebird'),
    _ = require('lodash'),
    Crawler = require('./lib/crawler'),
    utils = require('./lib/utils'),
    sailsMongo = require('sails-mongo'),
    dbStart = require('./lib/setupWaterline'),
    fs = require('fs'),
    jackrabbit = require('jackrabbit');

// Temp server for dev
var server = require('./serve_html');

function onDisconnect() {
    console.log('Disconnected');
}

function onError(err) {
    console.log('Rabbit error', err);
    return true;
}

function dbLoaded(wl, matcher) {
    console.log('Waterline initialized');
    var db = {
        Blog: wl.collections.blog,
        Post: wl.collections.post,
        RawImage: wl.collections.rawimage,
        Linker: matcher
    }

    // Pass in the database so that the crawler has access
    var crawler = new Crawler(db);
    // matcher.addMatch('http://shop.nordstrom.com/s/4080715', 'aLMyI')
    db.Blog.getRabbit().create('crawl.blog', {
        durable: true,
        noAck: false,
        expiration: 1000 * 30,
        messageTtl: 1000 * 60 * 60,
        prefetch: 1
    }, function(err, crawlQueue, queueInfo) {
        console.log('Crawl queue created');
        server(db, crawler);
        // crawlQueue.subscribe(crawler.crawl);
        // db.Post.queueAll();
        // db.Blog.updateAllFeeds();
    });

};

//////////////////////
// Waterline config //
//////////////////////

var models = {
    Blog: require('./models/Blog.js'),
    Post: require('./models/Post.js'),
    RawImage: require('./models/RawImage.js'),
    RawImageNative: function(cb) {
        return sailsMongo.native('blogdb', 'rawimage', cb)
    }

};

// var importer = require('./scripts/import');

function startWaterline(rabbit) {
    console.log('Starting Waterline');

    var waterlineOptions = {
        adapters: {
            'mongo': sailsMongo,
        },
        collections: {
            blog: models.Blog,
            post: models.Post,
            rawimage: models.RawImage
        },
        connections: {
            blogdb: {
                adapter: 'mongo',
                database: 'blogcrawler'
            }
        },
        globalMethods: {
            getRabbit: function() {
                return rabbit;
            }
        },
        globalAttributes: {
            problems: {
                type: 'array',
                required: false
            }
        }
    };

    Promise.all([dbStart(waterlineOptions), require('mined-linker')()])
        .spread(dbLoaded)
        // .catch(utils.say('Database load error %*#'));

}

function startRabbit() {
    var rabbit = jackrabbit(settings.rabbitBroker, 1)
        .on('error', onError)
        .on('disconnected', onDisconnect)
        .on('connected', function(err, something, rab) {
            console.log('Rabbit is connected');
            startWaterline(rabbit);
        });
}

//////////////////////
// Start everything //
//////////////////////

startRabbit();