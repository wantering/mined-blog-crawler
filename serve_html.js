//Lets require/import the HTTP module
var http = require('http'),
    _ = require('lodash'),
    handlebars = require('handlebars'),
    fs = require('fs'),
    url = require('url');

var raw = fs.readFileSync('./templates/post.hbs').toString();
var template = handlebars.compile(raw);

const styleTag = '<style>body {padding: 30px 50px; width: 3000px;} .node-read-div2p {display: inline-block;} img {max-width: 90%; margin: auto; left: 0; right: 0;} .rejects {background: #ef6666} .matches {background: #66ee66;} .hasProductLink {outline: 2px solid red;}</style>';

//Lets define a port we want to listen to
const PORT=3333; 

function parseURL (raw) {
    var u = url.resolve('http://dev.shopmined.com', raw);
    var parsed = url.parse(u,true);
    return {
        id: decodeURIComponent(parsed.pathname.substring(1)),
        skip: parsed.query.skip ? parseInt(parsed.query.skip) : null,
        rows: parsed.query.rows ? parseInt(parsed.query.rows) : null
    };
}


function processDoc (doc) {
    return {
        title: doc.title,
        permalink: doc.permalink,
        sources: [
            {
                name: 'Crawler',
                content: doc.content,
            },
            {
                name: 'Original',
                link: doc.permalink
            },
            {
                name: 'RSS',
                content: doc.feedBody
            }
        ],
        images: doc.images,
        links: doc.links,
        products: doc.products,
        productLinks: doc.productLinks.map(function(link) {
            return {
                link: link,
                title: link.split('://')[1].substring(0,50)
            }
        })
    }

}

function processContent (docs, second) {
    return {
        blog: docs && docs.length ? docs[0].blogName : 'No Results',
        posts: docs.map(processDoc)
    };
}

exports = module.exports = function (db, crawler) {
    //We need a function which handles requests and send response
    function handleRequest(request, response){
        var parsed = parseURL(request.url);
        if (isNaN(parseInt(parsed.id))) return;

        db.Post.crawlBlog(parsed.id, parsed.skip, parsed.rows, crawler)
            .then(processContent)
            .then(template)
            .then(function(html) {
                console.log('HTML');
                response.writeHead(200, {'Content-Type': 'text/html'});
                response.end( html );
            });
    }

    //Create a server
    var server = http.createServer(handleRequest);

    //Lets start our server
    server.listen(PORT, function(){
        //Callback triggered when server is successfully listening. Hurray!
        console.log("Server listening on: http://localhost:%s", PORT);
    });

}