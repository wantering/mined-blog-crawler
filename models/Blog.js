var rss = require('../lib/rss'),
    moment = require('moment'),
    utils = require('../lib/utils'),
    Promise = require('bluebird'),
    _ = require('lodash');
/**
 * Blog.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var serialize = {concurrency: 1};

exports = module.exports = {

    connection: 'blogdb',

    identity: 'blog',
    
    tableName: 'blogs',

    autoCreatedAt: false,
    
    autoUpdatedAt: false,

    autoPK: false,

    attributes: {
        id: { type: 'integer', primaryKey: true, unique: true },
        name: { type: 'string', required: true },
        feed: { type: 'string', unique: true, index: true },
        gender: { type: 'string', enum: ['m', 'f', 'n'], defaultsTo: 'n' },
        vip: { type: 'boolean', defaultsTo: false },
        lastPost: { type: 'string' },
        bloglovin: { type: 'string' },
        domain: { type: 'string' },
        lastProcessed: { type: 'datetime', index: true },
        followers: { type: 'integer', defaultsTo: 0 },

        makePost: function (doc) {
            if (!doc || !doc.permalink) {
                return Promise.reject('No permalink');
            }
            var Post = this.getCollection('post');

            return Post.findOrCreate({permalink: doc.permalink}, _.extend(doc, {blog: this.id, blogName: this.name}))
        },

        handleCompleteUpdate: function (results) {
            var lastPost = results[results.length - 1];
            if (lastPost && lastPost.permalink) {
                this.lastPost = lastPost.permalink;
            }
            this.lastProcessed = new Date();
            return this.save();
        },

        filterPosts: function(allPosts) {
            // Posts come out oldest first so that
            // if something goes wrong we don't leave posts hanging
            if (typeof this.lastPost == 'string') {
                var lastPostPosition = _.findIndex(allPosts, 'permalink', this.lastPost);

                if (lastPostPosition > -1) {
                    allPosts = allPosts.slice(lastPostPosition + 1);
                }
            }
            return allPosts;
        },

        updateFeed: function() {
            // console.log(post);
            
            return rss.process(this.feed)
                .bind(this)
                .then(this.filterPosts, this.saveError('feedSave', []) )
                .map(this.makePost, serialize)
                .then(utils.say('Added %length# items from ' + this.name))
                .then(this.handleCompleteUpdate)
                .catch(function(e) {
                    console.log('Post processing error', e, this);
                    // throw e;
                    return Promise.resolve();
                });
        }
    },

    updateOne: function (id) {
        return this.findOne({id: id})
            .then(_.method('updateFeed'));
    },

    updateAllFeeds: function () {
        return this.find({id: 1485})
               /* or: [
                    {
                        lastProcessed: {
                            '<': moment().subtract('6', 'hours').toDate()
                        }
                    },
                    {
                        lastProcessed: null
                    }
                ]
            })*/
            .limit(50)
            .sort('lastProcessed ASC')
            .then(function(results) {
                if (!results.length) {
                    return Promise.reject();
                }

                console.log(['Checking feeds at ', results.length, 'blogs. First is', results[0].name].join(' ') );
                
                return Promise.map(results, _.method('updateFeed'), serialize);
            })
            .then(this.updateAllFeeds.bind(this), utils.say('Error hit updating feed %*#'));
    },

    beforeCreate: function(obj, next) {
        // console.log('Creating ', obj);
        if (obj.id && typeof obj.id == 'number') {
            next();
            return;
        }

        this.find({where: {}, sort: '_id DESC', limit: 1})
            .then(function(docs){
                var lastID = docs.length ? docs[0].id : 0;
                obj.id = parseInt(lastID + 1);
                next();
            });
    }
};