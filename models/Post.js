/**
 * BlogPost.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
var Promise = require('bluebird'),
    utils = require('../lib/utils'),
    _ = require('lodash');

exports = module.exports = {
    migrate: 'alter',

    connection: 'blogdb',

    identity: 'post',
    
    tableName: 'posts',

    schema: true,

    attributes: {
        permalink: { type: 'string', index: true },
        otherURLs: { type: 'array' },
        title: 'string',
        blog: { type: 'integer', model: 'blog', via: 'id' },
        blogName: 'string',
        gender: {
            type: 'string',
            enum: ['m', 'f', 'n'],
            defaultsTo: 'n' 
        },
        feedBody: 'string',
        content: 'string',
        origin: 'string',
        feedCategories: 'string',
        images: 'array',
        links: 'array',
        productLinks: 'array',
        products: 'array',
        key: 'string',
        state: {
            type: 'string',
            enum: ['active', 'pending', 'crawled', 'hidden', 'error'],
            defaultsTo: 'pending'
        },
        
        queueCrawl: function () {
            var rabbit = this.getRabbit();
            return rabbit.publish('crawl.blog', {id: this.id});
        }

    },

    crawlOne: function (url, crawler) {
        return this.findOne({where: {id: '55fdc0277d0ef0c12b00cf15'}})
            .then(function(post) {
                console.log('Queueing crawl for ', post.permalink);
                return crawler.crawl(post);
            });
    },

    crawlBlog: function (id, skip, rows, crawler) {
        var Blog = this.getCollection('blog');
        return Blog.updateOne(id)
            .then(function() {
                return this.find({blog: parseInt(id)})
                    .limit(rows || 3)
                    .skip(skip || 0)
            }.bind(this))
            .then(function(posts) {
                console.log('About to crawl ' + posts.length + ' posts', id);
                return Promise.map(posts, crawler.crawl, {concurrency: 3});
            });      
        
    },

    queueAll: function() {
        return this.find({
                state: 'pending'
            })
            .limit(50)
            .sort('title ASC')
            .then(function(docs) {
                if (!docs || !docs.length) {
                    return Promise.reject()
                }
                docs.forEach(_.method('queueCrawl'));
                return true;
            })
            .then(utils.say('Added 50 posts to crawl queue'))
            .then(this.queueAll.bind(this), function() {
                console.log('Out of posts');
            })
    }
};