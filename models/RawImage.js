/**
 * RawImage.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */


var Promise = require('bluebird'),
    S3 = require('../lib/S3'),
    probe = Promise.promisify( require('probe-image-size') ),
    native = null,
    slashRE = /\//ig,
    request = require('request'),
    _ = require('lodash');

import { PIXEL_THRESHOLD } from '../lib/articleHelpers';

var getValue = _.curry(function(key, obj) {
    return _.isObject(obj) ? obj[key] : null;
});


exports = module.exports = {
    migrate: 'alter',

    connection: 'blogdb',

    identity: 'rawimage',
    
    tableName: 'rawimage',

    schema: true,

    attributes: {

        id: {
            type: 'string',
            primaryKey: true
        },

        pixels: {
            type: 'integer',
            index: true
        },

        key: {
            type: 'string'
        },

        upload: function () {
            console.log('Pretending to upload ' + this.id);
            this.key = String(+new Date());
            return this.save();
        }
    },

    fastCreate: function (url, pixels) {
        return this.ntv.updateAsync({_id: url},
                                   {$set: {
                                        _id: url,
                                        pixels: pixels
                                    }},
                                    {upsert: true})
                                    .return(pixels);
    },

    getSize: function (u) {
        var key = u.replace(slashRE, '|');
        // Start streaming the image
        var str = request.get({uri: u, encoding: null});
        // Inspect the first few bytes
        return probe(str)
            .then(function(result) {
                // => { width: xx, height: yy, type: 'jpg', mime: 'image/jpg' } 
                if (!result) {
                    console.log('Could not get image size', err);
                    str.destroy()
                    return 0;
                }

                var totalPixels = result.width * result.height;
                if (totalPixels < PIXEL_THRESHOLD) {
                    str.destroy();
                }

                return totalPixels;
            })
            .error(function(e) {
                if (e.code != 'ECONTENT') console.log('Image sizing error', e);
                if (str) str.destroy();
                return Promise.resolve(0);
            })
            .then( _.curry(this.fastCreate.bind(this))(u) );
    }

    
};