////////////////
// RSS Parser //
////////////////

var request = require('request'),
    Promise = require('bluebird'),
    FeedParser = require('feedparser'),
    Iconv = require('iconv').Iconv;

function getParams(str) {
    var params = str.split(';').reduce(function(params, param) {
        var parts = param.split('=').map(function(part) {
            return part.trim();
        });
        if (parts.length === 2) {
            params[parts[0]] = parts[1];
        }
        return params;
    }, {});
    return params;
}

function handleResponse (feedparser, res) {
    var stream = this,
        iconv, charset;

    if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

    charset = getParams(res.headers['content-type'] || '').charset;

    // Use iconv if its not utf8 already.
    if (!iconv && charset && !/utf-*8/i.test(charset)) {
        try {
            iconv = new Iconv(charset, 'utf-8');
            console.log('Converting from charset %s to utf-8', charset);
            iconv.on('error', done);
            // If we're using iconv, stream will be the output of iconv
            // otherwise it will remain the output of request
            stream = this.pipe(iconv);
        } catch (err) {
            this.emit('error');
        }
    }

    // And boom goes the dynamite
    stream.pipe(feedparser);
}


exports = module.exports = {

    process: function (url) {
        return new Promise(function(resolve, reject) {
            var req = request.get(url, {
                timeout: 10000,
                pool: false
            });

            req.setMaxListeners(100);
            // Some feeds do not respond without user-agent and accept headers.
            // req.setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36').setHeader('accept', 'text/html,application/xhtml+xml');

            var feedparser = new FeedParser();
            var postArray = []

            // Define our handlers
            req.on('error', reject);
            req.on('response', handleResponse.bind(req, feedparser));

            feedparser.on('error', reject);
            feedparser.on('readable', function() {
                var post;
                while (post = this.read()) {
                    var newPost = {
                        permalink: post.link,
                        title: post.title,
                        publishedAt: post.pubdate,
                        origin: 'rss',
                        feedCategories: post.categories ? post.categories.join(', ') : undefined,
                        feedBody: post.description,
                    }

                    if (post.image && post.image.url) {
                        newPost.feedBody += '<img src="' + post.image.url + '"/>'
                    }
                    postArray.unshift(newPost);
                }
            });

            feedparser.on('end', function() {
                resolve(postArray);
            });
        });
    }
}

