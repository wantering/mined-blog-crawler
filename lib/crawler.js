//////////////////
// BLOG CRAWLER ////////////////////

var _ = require('lodash'),
    Promise = require('bluebird'),
    request = Promise.promisifyAll( require('request') ),
    settings = require('../settings'),
    utils = require('./utils'),
    url = require('url'),
    fs = require('fs'),
    S3 = require('./S3'),
    Article = require('./article'),
    cheerio = require('cheerio');

/**
 * Saves a Post document to disk. Used for development
 * @param  {[type]} doc [description]
 * @return {[type]}     [description]
 */
function savePostInfo(doc) {
    return new Promise(function(resolve, reject) {
        if (doc) {
            var str = fs.createWriteStream('./data/' + doc.permalink.replace(/\W/ig, '') + '_processed.html');
            str.once('open', function() {
                str.write(doc.content);
                str.end();
                // console.log(JSON.stringify(postInfo, null, 4));
                resolve();
            });
        } else {
            reject();
        }
    })
}


/**
 * Sets default crawl options
 * @param  {String} reqUrl URL
 * @return {[type]}        [description]
 */

function makeCrawlOptions (reqUrl) {
    var j = request.jar();
    return {
        uri: reqUrl,
        followRedirect: true,
        maxRedirects: 5,
        timeout: 15000,
        jar: j,
        agent: false,
        headers: settings.headers
    };
};


/**
 * Triggers S3 upload and makes sure response status is satisfactory
 * @param  {[type]} res  [description]
 * @param  {[type]} body [description]
 * @return {[type]}      [description]
 */
function handleResponse (res, body) {
    if (!body || res.statusCode > 302) {
        return Promise.reject('Problem crawling. Status code is ' + res.statusCode);
    }

    var otherUrl = _.get(res, 'request.uri.href');
    if (otherUrl !== this.permalink) {
        this.otherURLs = (this.otherURLs || []).concat(otherUrl);
    }

    return S3.upload(this.permalink, body);
}


function extractImages ($) {
    var imgs = _.map( $('img').toArray(),'attribs.src')
        .map(function(src) {
            if (!src) return null;
            return formatSrc(src)
        })
        .filter(notNull);

    return _.uniq(imgs);
}


function prepDocument (key, body) {
    if (!body || !this.feedBody) {
        console.log(body, this.feedBody);
        return Promise.reject('Missing body');
    }

    this.key = key;
    var $ = cheerio.load(body);
    return $;
}

function getData (doc) {
    return request.getAsync( makeCrawlOptions(doc.permalink) )
        .bind(doc)
        .spread(handleResponse)
        .spread(prepDocument)
        .error(doc.saveError('crawl'))

}

exports = module.exports = function (db) {

    this.crawl = function (doc) {
        /**
         * Crawl method for RabbitMQ
         */
        return getData(doc)
            .then(function($) {
                return new Article($, doc.feedBody, url.parse(doc.permalink), db);
            })
            .then(function(extracted) {
                if (!extracted || !extracted.content) {
                    return Promise.reject('Did not extract');
                }
                _.keys(extracted).forEach(function(key) {
                    if (extracted[key]) {
                        doc[key] = extracted[key]
                    }
                });

                doc.state = 'crawled';
                return doc;
            })
            .then(utils.say('Successfully crawled %title#'), doc.saveError('extraction'))
            .then(_.method('save'))
            .catch(function(err) {
                console.log('Crawl error', err, doc.tite);
                return Promise.resolve();
            });
    };

    return this;

}