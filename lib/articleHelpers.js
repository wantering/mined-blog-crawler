/*
 * Url module. Required for resolving relative paths.
 */

var url = require("url"),
    _ = require('lodash');


/*
 * Regexp from origin Arc90 Readability.
 */


export
var REG = {
    unlikelyCandidates: /avatar|combx|cta|pager|comment|disqus|foot|header|menu|nav|rss|shoutbox|sidebox|sidebar|sponsor|share|bookmark|social|pin\-it\-button|skyscraper|advert|leaderboard|instapaper_ignore|entry-unrelated|pubexchange/ig,
    okMaybeItsACandidate: /article|^body|post\-body|column|main/ig,
    positiveRe: /article|\Wbody\W|content|entry|hentry|slide|page|main|image|pagination|post|text|detail|gallery|slideshow/i,
    negativeRe: /avatar|button|form|signup|login|combx|comment|captcha|cta|next|prev|contact|foot|widget|footer|footnote|link|newsletter|sidecontent|media|meta|promo|facebook|twitter|related|scroll|share|social|shoutbox|sponsor|utility|tag|tags\-wrapper|tip|dialog/i,
    divToPElementsRe: /<(blockquote|dl|div|img|ol|p|pre|table|ul)/i,
    replaceBrsRe: /(<br[^>]*>[ \n\r\t]*){2,}/gi,
    htmlStripper: /[\.,-\/#!$%\^&\*;:{}=\-_`~()\d]|\'|\“|\’|\’|\"|\”|\n|\?|\+|\&/ig,
    singleLetterRegex: /\s(\w)\s/ig,
    manySpacesRegex: /\s{2,}/ig,
    replaceFontsRe: /<(\/?)font[^>]*>/gi,
    badImage: /(static\.tumblr\.com|assets\.tumblr\.com|media\.tumblr\.com\/avatar|facebook\.com|gravatar|bloglovin|disqus|disquscdn|\/pin\/create|addthis\.com|\.gif|\.tiff)/i,
    badLink: /twitter\.com\/share|pinterest\.com\/pin\/create\//ig,
    singleSpacesRegex: /\s/ig,
    linkTLD: /\:\/\/(.*)\//i,
    trimRe: /^\s+|\s+$/g,
    normalizeRe: /\s{2,}/g,
    killBreaksRe: /(<br\s*\/?>(\s|&nbsp;?)*){1,}/g,
    urlDots: /\.(?!uk)/ig,
    videoRe: /http:\/\/(www\.)?(youtube|vimeo|youku|tudou|56|yinyuetai)\.com/i,
    nextLink: /(next|weiter|continue|>([^\|]|$)|»([^\|]|$))/i // Will be used to get the next Page of Article
};

const BAD_LINK_DOMAINS = [
    'facebook.com',
    'blogger.com',
    'imgur.com',
    'photobucket.com',
    'tumblr.com',
    'blogspot.com',
    'flickr.com',
    'wordpress.com',
    'addthis.com'
]

// Only these tags get scored directly. Score propagates to parents and grandparents
export
const DEFAULT_TAGS_TO_SCORE = "section,figure,h2,h3,h4,h5,h6,img,p,td,pre,span,li".split(",");

// Convert these to Ps
export
const DIV_TO_P_ELEMS = ["blockquote", "dl", "div", "ol", "p", "pre", "table", "ul"];

// Currently not used, but Mozilla's readability implements this
export
const ALTER_TO_DIV_EXCEPTIONS = ["DIV", "ARTICLE", "SECTION", "P"];

export
const PIXEL_THRESHOLD = 90000;

export
var notNull = _.negate(_.isNull);

/**
 * Node Types and their classification
 **/
export
var nodeTypes = {
    'mostPositive': ['div', 'article'],
    'positive': ['pre', 'td', 'blockquote', 'section'],
    'negative': ['address', 'dl', 'dd', 'dt', 'footer', 'header'],
    'mostNegative': ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'th', 'footer']
};


/**
 * Add the pixel count values to a Cheerio node
 * @param {Cheerio Node} $img   [description]
 * @param {Number} width  [description]
 * @param {Number} height [description]
 */
function addPixelCount($img, width, height) {
    var pixels = [width, height].map(parseFloat).reduce(multiply, 1);

    if (isNaN(pixels)) return false;
    $img.data('pixelcount', pixels);
    if (pixels > PIXEL_THRESHOLD) {
        $img.addClass('mined_big_img');
    }

    return true;
}


/**
 * Checks for matches with feed images and 
 */
export
function normalizeSrc($img, options) {

    var normalizedSrc = formatSrc($img.attr('src'), options.base);

    // Remove any images that end in the wrong extension
    if (REG.badImage.test(normalizedSrc)) {
        $img.data('badimg', true);
        return normalizedSrc;
    }

    $img.data('normalizedSRC', normalizedSrc);
    $img.attr('title', normalizedSrc);

    // imagesToFind includes images from the RSS feed
    // Very strong signal of quality if matches found
    var matchIndex = options.imagesToFind.indexOf(normalizedSrc)
    if (matchIndex > -1) {
        options.imagesToFind.splice(matchIndex, matchIndex + 1);
        $img.data('imagematch', true);
    }
    return normalizedSrc;
};

export
function getPixelCountSync($img) {
    var width = $img.attr('width');
    var height = $img.attr('height');

    return addPixelCount($img, width, height);
}

/**
 * Get Link density of this node.
 * Total length of link text in this node divided by the total text of the node.
 * Relative links are not included.
 **/

export
function getLinkDensity(node, $) {
    var links = node.find('a');
    var textLength = node.text().length;
    var linkLength = 0;

    links.each(function(index, elem) {
        var href = $(this).attr('href');
        if (!href || (href.length > 0 && href[0] === '#')) return;
        linkLength += $(this).text().length;
    });

    return (linkLength / textLength) || 0;
}


export
function cleanText(text) {
    if (typeof text != 'string') {
        return null;
    }
    text = text.replace(/(<([^>]+)>)/ig, "")
        .replace(REG.htmlStripper, "")
        .replace(REG.singleLetterRegex, '')
        .replace(REG.manySpacesRegex, " ")

    return text.toLowerCase().trim();
};


export
function multiply(a, b) {
    return a * b;
}

export
function filterLink (u, articleHost) {
    try {
        var host = u.split('://')[1].split('/')[0];
        var tld = host.split('.').slice(-2).join('.');
    }
    catch(e) {
        return false;
    }
    
    switch (true) {
        case (tld == articleHost):
        case (host == articleHost):
        case (u.indexOf('.jpg') > -1):
        case (BAD_LINK_DOMAINS.indexOf(tld) > -1):
            return false;

    }
    return true;
}

export
function addKeywords(node) {
    /**
     * Convenience function for adding a hidden keyword-text element to DOM nodes
     * @type {[type]}
     */
    var prev = findTextSibling(node, 'backwards');
    var after = findTextSibling(node, 'forwards');

    var text = [prev.text().substring(-150), after.text().substring(0, 150), (node.attr('alt') || '').substring(0, 150), (node.attr('title') || '')].join(' ');
    text = cleanText(text);
    if (text && text.length > 3) {
        node.attr('keyword-text', text);
    }

}


/**
 * Node Weight is calculated based on className and ID of the node.
 **/

export
function getClassWeight(node) {
    if (node == null || node.length == 0) return 0;
    var classAndID = (node.attr('class') || "") + (node.attr('id') || "");
    var weight = 0;

    if (node.get(0).name == "article") weight += 25;
    if (classAndID.search(REG.negativeRe) !== -1) weight -= 25;
    if (classAndID.search(REG.positiveRe) !== -1) weight += 25;

    return weight;
}


/**
 * Check the type of node, and get its Weight
 * Only run once per node
 **/

export
function initializeNode(node, options) {

    if (!node || node.length == 0) return 0;
    var tag = node.get(0).name;
    var weight = -1;

    if (nodeTypes['mostPositive'].indexOf(tag) > -1) weight += 6;
    if (nodeTypes['positive'].indexOf(tag) > -1) weight += 3;
    if (nodeTypes['negative'].indexOf(tag) > -1) weight -= 3;
    if (nodeTypes['mostNegative'].indexOf(tag) > -1) weight -= 6;

    weight += getClassWeight(node);

    node.attr('weight', weight);

    return weight;
};


export
function formatSrc(src, base) {
    if (typeof src != 'string') {
        return null;
    }
    if (src.substring(0, 2) === '//') {
        src = ('http:' + src);
    }
    if (base) {
        src = url.resolve(base, src);
    }
    return src;
    var parsed = url.parse(src);
    if (!parsed) return null;
    return 'http://' + parsed.host + parsed.path;
}


export
function findTextSibling(node, direction) {
    var directions = {
        backwards: 'prevUntil',
        forwards: 'nextUntil'
    };
    var action = directions[direction];
    var sib = node[action]();
    if (!sib.text().length) {
        sib = node.parent()[action]()
    }

    return sib;
};


/**
 * Evaluates a link and gives it a score
 * @param  {[type]} href [description]
 * @return {[type]}      [description]
 */
export
function scoreLink(articleHost, href, data) {
    var parsed = url.parse(href),
        score = 0;

    // console.log(articleHost, parsed.host);
    if (!parsed || !parsed.host) {
        return 0;
    }

    // Links to the homepage or root pages are bad
    if (parsed.host === articleHost && parsed.pathname < 10) score -= 1;

    // We filter out a few particularly annoying social links
    if (REG.badLink.test(href)) score -= 10;

    if (data.min_url) score += 5;

    if (data.short_key) score += 5;

    return score;
}

export
function upgradeImage(src) {

    if (!src || src == null) {
        console.log('No image URL provided')
        return null;
    }
    var img = src;

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // A bunch of little regex hacks to get to the largest possible image size from various sources //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    var replacers = [];
    switch (true) {
        //Blogger
        case _.contains(img, 'blogspot.com'):
        case _.contains(img, 'googleusercontent.com'):
            replacers.push([/\/s[0-9]{1,4}\//i, /s2400/]);
            break;
        
        //Wordpress
        case _.contains(img, 'files.wordpress.com'):
            replacers.push([/\&h=[0-9]{1,4}$/i, '']);
            replacers.push([/\?w=[0-9]{1,4}$/i, '']);
            break;


    }
    
    //Some old wordpress feeds have unique share images per post. Ugh.
    if (img.indexOf('http://feeds.wordpress.com/1.0/') > -1) {
        return null
    }
    //For the Timthumb plugin used on Peony Lim's site
    if (img.indexOf('/timthumb/') > -1 && img.match(/(src=)(.*)/i)[2]) {
        img = img.match(/(src=)(.*)/i)[2];
    }
    //More Wordpress
    if (img.indexOf('wp-content') > -1) {
        img = img.replace(/-[0-9]{1,4}x[0-9]{1,4}\.jpg/i, '.jpg');
        if (img.indexOf('thumb.php') > -1) {
            img = img.replace(/thumb\.xphp\?w=[0-9]{1,4}/i, 'thumb.php?w=2000');
        }
    }

    //Squarespace
    if (img.match(/static.*?\.squarespace/i) !== null) {
        img = img.replace(/\?.*/i, '');
        if (img.match(/(\.jpg|\.jpeg|\.png|\.tif)(\?|$)/) === null) img += '.jpg';
        // Experimenting with proxying Squarespace images to make them work
    }
    img = img.replace(/\?$/i, '');
    img = img.replace(/\.JPG$/, '.jpg');
    return img
};