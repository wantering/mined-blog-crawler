///////////////
// UTILITIES //
///////////////

var Promise = require('bluebird'),
    _ = require('lodash');

const spacerChar = '\\/';
const spacerEndRepeat = 2;
const spacerEnd = '/\\';

function getArgs (pseudo) {
    var args = [];
    for(var i = 0; i < pseudo.length; i++) {
        chunkSubstr(160, args, pseudo[i]);
    };
    return args;
}

var replacerRE = /\%(.*?)\#/ig

function chunkSubstr(size, memo, str) {
    if (typeof str !== 'string') {
        str = '';
    }
    var numChunks = (str.length / size + .5 | 0) || 1;
    // console.log(str, str.length, numChunks)
    for(var i = 0, o = 0; i < numChunks; ++i, o += size) {
        memo.push( str.substr(o, size) );
    }
}

function makeEven (val) {
    return Math.round(val/2) * 2;
}

exports = module.exports = {

    say: function (message) {
        return function (arg) {
            var chunks = [];
            chunks.push(message.replace(replacerRE, function(full, contents) {
                if (!arg) return '';
                if (contents && contents.indexOf('*') > -1) {
                    if (typeof arg === 'string') return arg;
                    if (arg.toString) {
                        return arg.toString();
                    }
                    if (_.isPlainObject(arg) ) {
                        chunks.push(JSON.stringify(arg, null, 4));
                    }
                    if (Array.isArray(arg)) {
                        chunks = chunks.concat(arg);
                    }
                    return '';
                }
                var contents = _.get(arg, contents);
                return typeof contents !== 'undefined' ? contents : '';

            }));

            console.log.apply(console, chunks);

            return arg;
        };
    },

    announce: function() {
        var args = getArgs(arguments)
        // console.log(args);

        var msg = args,
            maxLength = Math.max.apply(Math, msg.map(function(message) { return message.length } )),
            boxSize = makeEven(maxLength + 24),
            msgs = msg.map(formatLine).join('\n'),
            
            spacer = spacerEnd + Array(boxSize).join(' ') + spacerEnd + '\n',
            topAndBottom = Array( Math.round((boxSize / spacerChar.length ) + (spacerEnd.length ) )).join(spacerChar) + '\\',
            lines = ['\n\n' + topAndBottom,'\n' + spacer + msgs + '\n' + spacer + topAndBottom + '\n'];

        // console.log(boxSize);
        function formatLine (line, i) {
            if ((boxSize - line.length) % 2 != 0) line = line + ' ';

            function makeSpacer() {
                var l = ( (boxSize - line.length) / 2);
                return Array(Math.round(l)).join(' ');
            }

            return [spacerEnd].concat(makeSpacer(),line,makeSpacer() + ' ',spacerEnd).join('');
        }
        console.log.apply(console, lines);
        return null;
    }
}