var settings = require('../settings'),
    Promise = require('bluebird'),
    url = require('url'),
    request = require('request'),
    AWS = require('aws-sdk');

var S3Bucket = new AWS.S3({params: {Bucket: settings.s3Bucket}});
var putObject = Promise.promisify(S3Bucket.putObject, S3Bucket);
var slashRE = /\//ig;

function createKey (u) {
    /**
     * S3 Key Generator
     * @param  {[type]} url   [description]
     * @param  {Object} data) {               if (!data) return Promise.reject('No data to upload to S3');        var params [description]
     * @return {[type]}       [description]
     */
    return [u.replace(slashRE, '|'), +new Date()].join('/') + '.html';
}


function uploadImage (data, key) {
    if ( url.parse(data) ) {
        data = request.get()
    }
    var params = {
        Body: data,
        Key: key,
        ACL: 'public-read'
    };

    return putObject(params);
}


function upload (url, data) {
    if (!data) return Promise.reject('No data to upload to S3');
    var key = createKey(url);
    var params = {
        Body: data,
        ContentType: 'text/html',
        ContentLength: Buffer.byteLength(data),
        Key: key,
        ACL: 'public-read'
    };

    return putObject(params)
        .return([key, data]);
}


exports = module.exports = {
    
    upload: upload,

    uploadImage: uploadImage, 

    createKey: createKey

}