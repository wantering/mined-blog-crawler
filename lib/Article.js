var _ = require('lodash'),
    url = require('url'),
    S3 = require('./S3'),
    settings = require('../settings.js'),
    Promise = require('bluebird'),
    cheerio = require('cheerio');

import {
    REG,
    DEFAULT_TAGS_TO_SCORE,
    DIV_TO_P_ELEMS,
    ALTER_TO_DIV_EXCEPTIONS,
    PIXEL_THRESHOLD,
    notNull,
    nodeTypes,
    winksWhiteListDomains,
    winksRE,
    matchDomain,
    embellishImage,
    getLinkDensity,
    normalizeSrc,
    initializeNode,
    getPixelCountSync,
    cleanText,
    multiply,
    filterLink, 
    addKeywords,
    getClassWeight,
    findTextSibling,
    upgradeImage,
    scoreLink,
    formatSrc
} from './articleHelpers';

function prepDOM ($, options, db) {

    var asyncTasks = [];

    var nodesByLink = {};
    // Removing unnecessary nodes
    $(options.nodesToRemove).remove();
    
    // Add width and height to all images. Also removes tiny ones
    // Important to do this early, as further steps give preference to nodes with image children
    $('img').each(function(i, el) {
        var $el = $(this);
        var normalized = normalizeSrc($el, options);

        if (normalized && !$el.data('badimg')) {
            asyncTasks.push( checkImageSize($el, normalized, db, options.bigImages) );
        }

        addKeywords($el);
    });
    

    var allUrls = $('a').map(function(i, el) {
        var $el = $(el);
        var href = $el.attr('href');
        if (!href) return null;

        var normalized = formatSrc(href, options.base);
        if (!normalized || !filterLink(normalized, options.host) ) return null;

        // Create a working set of DOM nodes indexed by URL
        nodesByLink[normalized] = nodesByLink[normalized] ? nodesByLink[normalized].concat($el) : [$el];
        return normalized;
    })
    .toArray()
    .filter(notNull);

    options.allLinks = options.allLinks.concat(allUrls);

    var filteredUrls = _.uniq(_.filter(allUrls, db.Linker.validateDomain));
    filteredUrls.forEach(function(u) {
        options.urlScores[u] = 2;
    });

    var urlTask = Promise.map( filteredUrls , db.Linker.sendLink, {concurrency: 20} )
        .then(function(processedUrls) {
            _.reject(processedUrls, 'min_url', null).forEach(function(result) {
                if (result && result.min_url) {
                    if (!nodesByLink[result.original]) {
                        return console.log('could not find matching node for link ', result.original);
                    }
                    options.productLinks.push(result.original);
                    if (result.short_key) {
                        options.products.push(result.short_key);
                    }
                    nodesByLink[result.original].forEach(function(node) {
                        node.attr('linkscore', result.short_key ? 6 : 4);
                        node.addClass('hasProductLink');
                        node.closest('div,section,td,li').addClass('containsProduct');  
                        if (result.short_key) {
                            node.attr('shortKey', result.short_key);  
                        }
                    })
                }
            });
            return Promise.resolve();
        });
    asyncTasks.push(urlTask);

    return Promise.all(asyncTasks);
}

var Article = function($, feedBody, uri, db) {
    /**
     * Returns a promise that will return a processed article
     * @param {Cheerio root} $ Full body
     * @param {String} feedBody [optional] string of the article from a RSS feed
     * @param {Object} uri parsed URL object for the base of the page
     * @type {[type]}
     */
    if (!$ || !uri || !uri.path) {
        throw new Error('Missing key fields to extract the article');
    }
    var self = this;
    this.$ = $;
    this.cache = {};
    
    this.base = url.format(uri);


    var options = {
        considerDIVs: true,
        host: uri.hostname,
        base: this.base,
        path: uri.pathname,
        href: uri.href,
        nodesToRemove: 'meta,iframe,noscript,style,object,script',
        imagesToFind: [],
        textToMatch: [],
        bigImages: [],
        urlScores: {},
        productLinks: [],
        allLinks: [],
        products: []

    };

    if (feedBody) {
        options = _.extend(options, findImportantElementsFromFeed(feedBody, this.base));
    }
    
    this.options = options;

    return prepDOM(this.$, this.options, db)
        .then(this.finalOutput.bind(this))
    
}

Article.prototype.finalOutput = function () {
    console.log('Generating final output');
    var article = generateCleanVersion(this.$, this.options);
    return {
        content: article.html(),
        links: this.options.allLinks,
        productLinks: this.options.productLinks,
        products: this.options.products,
        images: this.options.bigImages,
    }
}

function extractImages ($) {
    /**
     * Creates an array of unique image values
     * @type {[type]}
     */
    var imgs = _.map( $('img').toArray(),'attribs.src')
        .map(function(src) {
            if (!src) return null;
            return formatSrc(src)
        })
        .filter(notNull);

    return _.uniq(imgs);
}


function resolveLinks ($arr, targetAttr, base) {
    /**
     * Takes a Cheerio faux-array and returns a nicely pruned set of links
     */

    if (!$arr || !$arr.length) {
        return [];
    }
    var ret = [];

    // $arr.each(function(i, el) {
    //     if (!link || !base) {
    //         return null;
    //     }
    //     return url.resolve(base, link);
    // });
    return ret;
}


// Article.prototype.getImages = function() {
//     var content = this.content;
//     var realArray = [];
//     var $ = this.$;

//     return resolveLinks( $(content).find('img'), 'attribs.src',  this.uri);

//     // return realArray.map(utils.image);
// }


Article.prototype.getText = function() {
    var content = this.content;
    var $ = this.$;
    var text = $(content).text();
    if (text && text.length) return text.slice(0, 500);
    else return undefined;
}


// Article.prototype.getLinks = function() {
//     try {
//         var content = this.dom;
//         var $ = this.$;

//         return resolveLinks( $(content).find('a'), 'attribs.href',  this.uri);
        
//     } catch (e) {
//         console.log(e)
//     }
// }


Article.prototype.getHTML = function() {
    return this.$.html();
}

function getPixelCountAsync ($el, url) {

}

function checkImageSize ($el, u, db, imageArray) {
    return db.RawImage.ntv.findOneAsync({_id: u})
        .then(function(res) {
            if (res && res.pixels) {
                // console.log('Image found in DB for ' + url + ': ', res);
                return res.pixels;
            }

            return db.RawImage.getSize(u);
        })
        .then(function(pixels) {
            if (pixels > PIXEL_THRESHOLD) {
                imageArray.push(u);
                $el.addClass('mined_big_img');
            }
            else {
                $el.data('badimg', true);
            }
            return $el;
        });
}


/**
 * Traverse all Nodes and remove unlikely Candidates.
 **/

function getCandidates($, options) {

    // Candidates Array
    var candidates = [];

    // Iterate over all Nodes in body
    $('*','body').each(function(index, element) {
        var node = $(this);

        // If node is null, return, otherwise Illegal Access Error
        if (!node || node.length == 0) return;
        var nodeType = node.get(0).name;
        
        // Remove Unlikely Candidates
        var classAndID = (node.attr('class') || "") + (node.attr('id') || "");
        
        // This is the danger zone!!!!
        var negativeMatches = classAndID.match(REG.unlikelyCandidates);
        var positiveMatches = classAndID.match(REG.okMaybeItsACandidate);
        if ( negativeMatches && (!positiveMatches || negativeMatches.length > positiveMatches.length) && !node.has('.mined_big_img').length ) {
            // console.log('Removing ')
            node.remove();
        }
        
        var nodeText = node.text().length ? node.text() : (node.attr('alt') || '');
        nodeText = nodeText.trim().toLowerCase();

        // Remove Elements that have no children and have no content
        if (nodeType == "div" && node.children().length < 1 && nodeText.length < 1 && !node.find('.mined_big_img').length) {
            node.remove();
            return true;
        }

        // Remove Style 
        node.removeAttr('style');

        // turn all divs that don't have children block level elements into p's
        if ( DIV_TO_P_ELEMS.indexOf(nodeType) > -1 && options.considerDIVs ) {
            // The node contains one of these elements
            var nodeHTML = node.html();
            if ( nodeHTML.search(REG.divToPElementsRe) === -1 ) {
                node.replaceWith('<p class="node-read-div2p">' + nodeHTML + '</p>');
            } else {
                node.contents().each(function(index, element) {
                    var child = $(this),
                        childEntity;
                    if (!child || (!child.get(0))) {
                        return;
                    }
                    childEntity = child.get(0);
                    if (childEntity.type == 'text' && childEntity.data && childEntity.data.replace(REG.trimRe, '').length > 1) {
                        if (childEntity.type !== 'img') {
                            child.replaceWith('<p class="node-read-div2p">' + childEntity.data + '</p>');
                        }
                    }
                });
            }
        }

        // score paragraphs.
        if (DEFAULT_TAGS_TO_SCORE.indexOf(nodeType) > -1) {
            calculateNodeScore($, node, candidates, options);
        }

        // Clean the headers
        if (["h1", "h2", "h3", "h4", "h5", "h6"].indexOf(nodeType) !== -1) {
            var weight = getClassWeight(node, $);
            var density = getLinkDensity(node, $);
            if (weight < 0 || density > 0.2) {
                node.remove();
            }
        }


        return true;
    });

    // calculate scores of `P`s that were turned from DIV by us.
    $('p.node-read-div2p').each(function() {
        calculateNodeScore($, $(this), candidates, options);
    });

    return candidates;
}


function scoreCandidate(node, contentScore, candidates, options) {
    /**
     * Give a score to each node based on the Content.
     * @param node
     * @param contentScore
     * @param candidates
     */
    
    var score;
    if (typeof node.data('readabilityScore') == "undefined") {
        score = initializeNode(node, options);
        candidates.push(node);
    } else {
        score = node.data('readabilityScore') || 0;
        // console.log('Candidate already has a score', score);
    }
    node.attr('score', score + contentScore);
    node.data('readabilityScore', score + contentScore)
}

/**
 * calculate score of specified node.
 * @param node
 * @param candidates
 */
function calculateNodeScore($, node, candidates, options) {
    var txt = node.text(),
        contentScore = 1;

    var childImages = node.is('img') ? node : node.find('img');

    var childLinks = (node.is('.hasProductLink') ? node : node.find('.hasProductLink') ).map(function(i, el) {
        var score = _.get(el, 'attribs.linkScore');
        return score;
    })


    if (childLinks.length) {
        // Map all child links to a score then add them all together 
        var linkScore = _.reduce(childLinks, _.add, 0);
        node.attr('aggLinkScore', linkScore);

        contentScore += Math.min(linkScore, 10)
    }

    // Ignore too small nodes
    if (txt.length < 25 && !node.is('img') && !node.find('img').length && !node.find('a').length) return;

    var matchIndex = options.textToMatch.indexOf( cleanText(txt) );
    
    // Big bonus points for matching content from the RSS feed
    if (matchIndex > -1) {
        options.textToMatch.splice(matchIndex, matchIndex + 1);
        node.addClass('matches');
        contentScore += 10;
    }

    // Add points for any commas within this paragraph
    // support Chinese commas.
    var commas = txt.match(/[,，.。;；?？、]/g);
    if (commas && commas.length) {
        contentScore += Math.min(commas.length, 5);
    }

    // For every 100 characters in this paragraph, add another point. Up to 3 points.
    contentScore += Math.min(Math.floor(txt.length / 100), 3);

    if (childImages && childImages.length) {
        // Check to see if there is a large image in the node
        var imageScore = 0;

        childImages.each(function(i, el) {
            var $el = $(el);

            if ($el.data('imagematch')) {
                $el.addClass('matches');
                imageScore += 3
            } else if ($el.data('badimg')) {
                imageScore -= 2
                $el.remove();
            }
            
            if ($el.hasClass('mined_big_img') ) {
                imageScore += 6;
            }

        });
        contentScore += Math.min(imageScore, 30);
    }

    // Initialize Parent and Grandparent
    // First initialize the parent node with contentScore / 1, then grandParentNode with contentScore / 2
    var parent = node.parent();

    if (parent && parent.length > 0) {
        scoreCandidate(parent, contentScore, candidates, options);
        var grandParent = parent.parent();
        if (grandParent && grandParent.length > 0) {
            scoreCandidate(grandParent, contentScore / 2, candidates, options);
        }
    }
    node.attr('contentscore', contentScore);
    return contentScore
}


/**
 * Filter TopCandidate Siblings (Children) based on their Link Density, and readabilityScore
 * Append the nodes to articleContent
 **/

function filterCandidates(topCandidate, siblings, $) {
    var articleContent = $("<div id='readabilityArticle'></div>");
    var rejects = $("<div class='rejects'><h1>Rejected</h1></div>");
    var siblingScoreThreshold = Math.max(10, topCandidate.data('readabilityScore') * 0.15);
    articleContent.attr('sibling-threshold', siblingScoreThreshold);
    siblings.each(function(index, elem) {

        var node = $(this);
        var append = false;
        var type = node.get(0).name;
        var score = node.data('readabilityScore');

        var hasBigImage = node.is('.mined_big_img') || node.find('.mined_big_img').length;
        var hasProductLink = node.is('.hasProductLink') || node.find('.hasProductLink').length;

        var children = siblings.contents().length;

        if (hasBigImage || hasProductLink || node.is(topCandidate) || score > siblingScoreThreshold) {
            articleContent.append(node);
            return true;

        }

        if (children > 0) {
            node.children().each(function(index, cont) {
                var $child = $(this);
                if ($child.find('.mined_big_img').length || $child.is('.mined_big_img') ){
                    append = true;
                    return;
                }
                if ($child.text().trim().length < 1) {
                    $child.addClass('siblingContent');
                    rejects.append($child.clone());
                    $child.remove();
                }
            });
        }

        if (!append && (['p', 'ul', 'ol', 'blockquote', 'pre', 'h2'].indexOf(type) > -1) ) {
            $(elem).find('a').each(function(index, elems) {
                var text = $(elems).text();
                if (text.length > 1 || $(elems).find('.mined_big_img').length) return;

                if (elems.name == "a") {
                    rejects.append($(elems).clone());
                    $(elems).remove();
                }
            });

            var linkDensity = getLinkDensity(node, $);
            var text = node.text() || node.attr('alt') || '';
            
            var len = text.length;
            if (len < 3) {
                if (!children && !hasBigImage) {    
                    rejects.append($(node).clone());
                    $(node).remove();
                    return;
                }
            }

            if (len > 80 && linkDensity < 0.25) {
                append = true;
            } else if (len < 80 && linkDensity == 0 && node.text().replace(REG.trimRe, "").length > 0) {
                append = true;
            }
        }
        if (append) {
            articleContent.append(node);
        }
        else {
            rejects.append($(node).clone());
        }

    });
    
    articleContent.append(rejects);
    return articleContent;
}

/**
 * Select the TopCandidate from all possible candidates
 **/

function getBodyContent (candidates, $, options) {
    var topCandidate = null;

    candidates.forEach(function(elem) {
        var linkDensity = getLinkDensity(elem, $);
        var score = elem.data('readabilityScore');
        var siblings = elem.children('p,img,div,section').length;

        elem.data('readabilityScore', Math.min(10, Math.max(siblings, 1)) * score * (1 - linkDensity));
        elem.attr('readabilityScore', Math.min(10, Math.max(siblings, 1)) * score * (1 - linkDensity));
        if (!topCandidate || elem.data('readabilityScore') > topCandidate.data('readabilityScore')) {
            topCandidate = elem;
        }
    });

    /**
     * If we still have no top candidate, just use the body as a last resort.
     * Should not happen.
     **/
    if (topCandidate === null) {
        return $.root();
    }

    // Perhaps the topCandidate is the parent?
    var parent;
    if (!(parent = topCandidate.parent()) || parent.length == 0 || topCandidate.children('p, div','article','section','img').length > 5) {
        return filterCandidates(topCandidate, topCandidate.children(), $);
    } else {
        return filterCandidates(topCandidate, parent.children(), $);
    }
}

function extractTextNodes ($) {
    var nodes = _.reduce($('*').toArray(), function (memo, node, val) {
        // console.log(memo, node, val)
        var text = $(node).text();
        if (text && text.length > 25) {
            memo.push(cleanText(text))
        }
        return memo
    },[])
    .filter(function(text) {
        return text && text.length > 25;
    });

    return _.uniq(nodes);
}

function generateCleanVersion ($, options) {
    var candidates = getCandidates($, options);

    var article = getBodyContent(candidates, $, options);
    if (article.length < 1) {
        article = getBodyContent([$('body')], $, options)
    }

    return article.wrap('<div class="minedExtracted"></div>');
};

/**
 * [Mark takes a string of a blog post from a feed and adds it to the object]
 * @param  {[type]} feedBody [description]
 * @param  {[type]} baseURL  [description]
 * @return {[type]}          [description]
 */
function findImportantElementsFromFeed (feedBody, baseURL) {
    var $feed = cheerio.load(feedBody, {normalizeWhitespace: true});
    return {
        textToMatch: extractTextNodes($feed),
        imagesToFind: extractImages($feed, baseURL)
    }
}

Article.prototype.resolveLink = function (url) {
    return url.resolve(this.base, url);
}


exports = module.exports = Article;