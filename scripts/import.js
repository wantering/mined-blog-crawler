var Promise = require('bluebird'),
    fs = require('fs');

var genderMap = {
    male: 'm',
    female: 'f',
    neutral: 'n'
};

exports = module.exports = function(wl) {
    var Blog = wl.collections.blog;
    console.log('Importer running');
    fs.readFile('./scripts/blogs.json', function(err, str) {
        var blogs = JSON.parse(str);
        console.log(blogs[0]);

        return Promise.map(blogs, function(oldblog, i) {
            if (!oldblog.feed || oldblog.feed === 'null') return Promise.resolve();

            return Blog.create({
                name: [oldblog.name.first,oldblog.name.last].join(' ').trim(),
                feed: oldblog.feed,
                followers: oldblog.followers || 0,
                gender: genderMap[oldblog.gender] || 'n',
                bloglovin: oldblog.bloglovinId
            })
            .then(function(newblog) {
                console.log('Created ', newblog);
            }, function (e) {
                console.log(e);
            })
        }, {concurrency: 1})
    });
}